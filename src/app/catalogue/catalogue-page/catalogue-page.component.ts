import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonsService } from 'src/app/services/pokemons.service';
import { SelectedPokemonService } from 'src/app/services/selected-pokemon.service';
import { UpdatePokemonService } from 'src/app/services/update-pokemon.service';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css'],
})
export class CataloguePageComponent implements OnInit {
  constructor(
    private router: Router,
    private readonly pokemonService: PokemonsService,
    private readonly selectedPokemonService: SelectedPokemonService,
    private readonly updatePokemonService: UpdatePokemonService
  ) {
    //If localstorage is not set redirects user to login
    if (localStorage.getItem('pkmtr') === null) {
      this.router.navigate(['/login']);
    }
  }
  //Getting the pokemons from the API
  ngOnInit(): void {
    this.pokemonService.fetchPokemons();
  }
  get pokemons(): Pokemon[] {
    return this.pokemonService.getPokemons();
  }

  //Getting the selected pokemon from the catalogue
  get pokemon(): Pokemon | null {
    return this.selectedPokemonService.pokemon();
  }
  onPokemonClicked(pokemon: Pokemon): void {
    this.selectedPokemonService.setPokemon(pokemon);
  }

  //Updating the users collected Pokèmon
  public updatePokemon(id: String, url: String): void {
    this.updatePokemonService.updatePokemon(id, url);
    alert(`You have collected ${id}`);
  }
}
