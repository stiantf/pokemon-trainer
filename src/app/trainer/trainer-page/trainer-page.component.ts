import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Username } from 'src/app/models/username.model';
import { GetPokemonService } from 'src/app/services/get-pokemon.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css'],
})
export class TrainerPageComponent implements OnInit {
  //If localstorage is not set redirects user to login
  constructor(
    private router: Router,
    private readonly getPokemonService: GetPokemonService
  ) {
    if (localStorage.getItem('pkmtr') === null) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit(): void {
    this.getPokemonService.fetchPokemon();
  }
  get pokemons() {
    return this.getPokemonService.getPokemon();
  }

  //Clears localstorage and returns to loginsite
  public onLogout(): void {
    localStorage.removeItem('pkmtr');
    this.router.navigate(['/']);
  }
}
