import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CataloguePageComponent } from './catalogue/catalogue-page/catalogue-page.component';
import { LoginComponent } from './login/login/login.component';
import { PageNotFoundComponent } from './PageNotFound/page-not-found/page-not-found.component';
import { TrainerPageComponent } from './trainer/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'trainer',
    component: TrainerPageComponent
  },
  {
    path: 'catalogue',
    component: CataloguePageComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
