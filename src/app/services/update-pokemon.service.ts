import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class UpdatePokemonService {
  private url: string = '';
  private id: string = '';
  private error: string = '';

  constructor(private readonly http: HttpClient) {}

  private user = localStorage.getItem('pkmtr');

  // Updating the users selected pokemon
  public updatePokemon(id: String, url: String): void {
    this.http
      .put<Pokemon>(`http://localhost:3000/users/${this.user}`, {
        pokemon: id,
        url: url,
      })
      .subscribe(
        (data) => {
          this.id = data.id;
          this.url = data.url;
        },
        (error: HttpErrorResponse) => {
          this.error = error.message;
        }
      );
  }
}
