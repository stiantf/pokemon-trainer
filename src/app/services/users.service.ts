import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Username } from "../models/username.model";

@Injectable({
    providedIn: 'root'
})

//This class will update the database with a new user!
export class UsersService {
    private error: string = '';
    private username: string = '';

    constructor(private readonly http: HttpClient) {
    }

    // POST request to add user to database
    public addUser(id: Username): void {
        //Default the user will have pikachu as their pokèmon
        this.http.post<Username>('http://localhost:3000/users', {id, "pokemon":"pikachu", "url":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/25.png"}).subscribe(data => {
            this.username = data.id
        }, (error: HttpErrorResponse) => {
            this.error = error.message
        })
    }
}