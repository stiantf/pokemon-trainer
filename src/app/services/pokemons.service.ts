import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonsService {
  private error: string = '';
  private pokemons: Pokemon[] = [];

  constructor(private readonly http: HttpClient) {}

  //Fetching the Pokèmons from the API
  public fetchPokemons(): void {
    this.http.get<Pokemon[]>('http://localhost:3000/pokemons').subscribe(
      (pokemons: Pokemon[]) => {
        this.pokemons = pokemons;
      },
      (error: HttpErrorResponse) => {
        this.error = error.message;
      }
    );
  }
  //Returning pokemons
  public getPokemons(): Pokemon[] {
    return this.pokemons;
  }
}
