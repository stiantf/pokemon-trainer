import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { Username } from '../models/username.model';

@Injectable({
  providedIn: 'root',
})
export class GetPokemonService {
  private pokemons: Pokemon[] = [];
  private user = localStorage.getItem('pkmtr');
  private error: string = '';
  private pokemon: string = '';
  private url: string = '';
  constructor(private readonly http: HttpClient) {}

  //Fetching the users collected pokemon from the API
  public fetchPokemon(): void {
    this.http
      .get<Username>(`http://localhost:3000/users/${this.user}`)
      .subscribe(
        (data) => {
          this.pokemon = data.pokemon;
          this.url = data.url;
        },
        (error: HttpErrorResponse) => {
          this.error = error.message;
        }
      );
  }

  //returning the collected pokemon
  public getPokemon() {
    return [this.pokemon, this.url];
  }
}
